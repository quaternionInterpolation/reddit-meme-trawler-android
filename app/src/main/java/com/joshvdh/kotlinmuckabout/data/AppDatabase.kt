package com.joshvdh.kotlinmuckabout.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.joshvdh.kotlinmuckabout.data.models.PokemonData

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
@Database(entities = [PokemonData::class],
    version = 1)
abstract class AppDatabase: RoomDatabase() {

}